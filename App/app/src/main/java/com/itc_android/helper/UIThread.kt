package com.itc_android.helper

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * The UI Thread
 */
class UIThread @Inject internal constructor(){

    fun getScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}
