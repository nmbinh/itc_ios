package com.itc_android.repository

import android.content.Context
import android.support.v7.preference.PreferenceManager
import com.itc_android.entity.AppSettings


/**
 * Created by binhnguyen on 9/17/17.
 */
class AppSettingsRepository constructor(private val mContext: Context) {

    fun getAppSettings(): AppSettings {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext)
        var value = sharedPref.getString("numOfResultsToShow", "5")
        value = value.replace("[^0-9]".toRegex(), "")
        val numOfResults = value.toInt()
        val settings = AppSettings()
        settings.numOfResults = numOfResults
        return settings
    }
}