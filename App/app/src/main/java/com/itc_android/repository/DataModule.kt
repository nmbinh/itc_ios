package com.itc_android.repository

import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by binhnguyen on 9/17/17.
 */
@Module
class DataModule(private val mContext: Context) {
    @Provides
    fun provideAppSettingsRepository(): AppSettingsRepository {
        return AppSettingsRepository(mContext)
    }
}