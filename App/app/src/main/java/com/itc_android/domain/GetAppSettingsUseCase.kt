package com.itc_android.domain

import com.itc_android.entity.AppSettings
import com.itc_android.repository.AppSettingsRepository
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by binhnguyen on 9/17/17.
 */
class GetAppSettingsUseCase @Inject constructor() : BaseUseCase<AppSettings, Unit>() {
    @Inject lateinit var mRepository: AppSettingsRepository
    override fun buildUseCaseObservable(input: Unit): Observable<AppSettings> {
        return Observable.create {
            it.onNext(mRepository.getAppSettings())
            it.onComplete()
        }
    }
}