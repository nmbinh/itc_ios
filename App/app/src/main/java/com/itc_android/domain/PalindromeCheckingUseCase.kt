package com.itc_android.domain

import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by binhnguyen on 9/16/17.
 */
class PalindromeCheckingUseCase @Inject constructor() : BaseUseCase<Boolean, String>() {
    override fun buildUseCaseObservable(input: String): Observable<Boolean> {
        return Observable.just(input)
                .map(this::removeInvalidCharacters)
                .map { it.toLowerCase() }
                .map(this::checkPalindrome)
    }

    private fun removeInvalidCharacters(input: String): String {
        return input.replace("[^A-Za-z0-9]".toRegex(), "")
    }

    private fun checkPalindrome(input: String): Boolean {
        var head = 0
        var tail = input.length - 1
        while (tail > head) {
            if (input[head] != input[tail]) {
                return false
            }
            head++
            tail--
        }
        return true
    }
}