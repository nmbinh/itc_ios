package com.itc_android.domain

import com.itc_android.helper.JobExecutor
import com.itc_android.helper.UIThread
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by binhnguyen on 9/16/17.
 */
abstract class BaseUseCase<RESPONSE_DATA, INPUT_DATA> {
    @Inject lateinit var threadExecutor: JobExecutor
    @Inject lateinit var postExecutionThread: UIThread

    /**
     * Builds an [Observable] which will be used when executing the current use case.
     */
    abstract fun buildUseCaseObservable(input: INPUT_DATA): Observable<RESPONSE_DATA>


    fun run(params: INPUT_DATA): Observable<RESPONSE_DATA> {
        return buildUseCaseObservable(params).compose(applyThread())
    }

    fun applyThread(): ObservableTransformer<RESPONSE_DATA, RESPONSE_DATA> {
        return ObservableTransformer { stream ->
            stream.subscribeOn(Schedulers.from(threadExecutor))
                    .observeOn(postExecutionThread.getScheduler(), true)
        }
    }
}