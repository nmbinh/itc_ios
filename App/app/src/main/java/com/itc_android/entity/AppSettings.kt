package com.itc_android.entity

/**
 * Created by binhnguyen on 9/17/17.
 *
 * In real application, this entity should define much more setting properties.
 */
class AppSettings {
    var numOfResults: Int = 5
    var showResultImmediately = false
}