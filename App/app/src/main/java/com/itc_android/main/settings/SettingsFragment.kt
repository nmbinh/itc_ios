package com.itc_android.main.settings

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.preference.EditTextPreference
import android.support.v7.preference.Preference
import android.support.v7.preference.PreferenceFragmentCompat

import com.itc_android.R

class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences)
        preferenceManager.sharedPreferences.all.onEach {
            findPreference(it.key)?.let {
                showValueOfEditTextSettings(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        preferenceManager.sharedPreferences
                .registerOnSharedPreferenceChangeListener(this)
    }

    override fun onPause() {
        preferenceManager.sharedPreferences
                .unregisterOnSharedPreferenceChangeListener(this)
        super.onPause()
    }

    override fun onSharedPreferenceChanged(sharePref: SharedPreferences?, key: String?) {
        //Display value under settings title:
        findPreference(key)?.let {
            showValueOfEditTextSettings(it)
        }

        LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(APP_SETTINGS_CHANGED_BROADCAST))
    }

    private fun showValueOfEditTextSettings(pref: Preference) {
        if (pref is EditTextPreference) {
            val numericValue = pref.text.replace("[^0-9]".toRegex(), "")
            pref.summary = numericValue
            pref.text = numericValue
        }
    }

    companion object {
        val APP_SETTINGS_CHANGED_BROADCAST = "APP_SETTINGS_CHANGED_BROADCAST"
    }
}