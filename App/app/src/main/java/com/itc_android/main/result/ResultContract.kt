package com.itc_android.main.result

import com.itc_android.entity.Palindrome
import com.itc_android.main.result.item.ResultItemViewAdapter
import com.itc_android.main.result.item.ResultItemViewModel
import com.itc_android.scope.FragmentScope
import dagger.Provides
import dagger.Subcomponent

/**
 * Created by binhnguyen on 9/16/17.
 */
interface ResultContract {
    interface View {
        fun refreshTheResultList()
    }

    interface Presenter {
        fun onStart()
        fun onReceiveNewResultItem(item: Palindrome)
        fun loadAppSettings()
    }

    @FragmentScope
    @Subcomponent(modules = arrayOf(Module::class))
    interface Component {
        fun inject(fragment: ResultFragment)
    }

    @dagger.Module
    class Module(val mView: View) {
        @FragmentScope
        @Provides
        fun provideView() = mView

        @FragmentScope
        @Provides
        fun providePresenter(it: ResultPresenter): Presenter = it

        @FragmentScope
        @Provides
        fun provideResultItemViewAdapter() = ResultItemViewAdapter()
    }
}