package com.itc_android.main.result.item

import com.itc_android.entity.Palindrome

/**
 * Created by binhnguyen on 9/17/17.
 */
class ResultItemViewModel(entity: Palindrome) {
    var statement: String = entity.statement

    //we need a string to be displayed
    var isPalindrome: String = entity.isValid.toString().capitalize()
}