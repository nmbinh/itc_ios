package com.itc_android.main.input

import com.itc_android.scope.FragmentScope
import dagger.Provides
import dagger.Subcomponent

/**
 * Created by binhnguyen on 9/16/17.
 */
interface InputContract {
    interface View {
        fun showInvalidInputError()
    }

    interface Presenter {
        fun checkPalindrome(string: String)
    }

    @FragmentScope
    @Subcomponent(modules = arrayOf(Module::class))
    interface Component {
        fun inject(fragment: InputFragment)
    }

    @dagger.Module
    class Module(val mView: View) {
        @FragmentScope
        @Provides
        fun provideView() = mView

        @FragmentScope
        @Provides
        fun providePresenter(it: InputPresenter): Presenter = it
    }
}