package com.itc_android.main.result

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itc_android.R
import com.itc_android.databinding.ResultFragmentBinding
import com.itc_android.entity.Palindrome
import com.itc_android.main.MainActivity
import com.itc_android.main.result.item.ResultItemViewAdapter
import com.itc_android.main.settings.SettingsFragment
import javax.inject.Inject


/**
 * Created by binhnguyen on 9/16/17.
 */
class ResultFragment : Fragment(), ResultContract.View {
    @Inject lateinit var mPresenter: ResultContract.Presenter
    @Inject lateinit var mItemViewAdapter: ResultItemViewAdapter

    private lateinit var mBinding: ResultFragmentBinding
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        injectDependencies()
        LocalBroadcastManager.getInstance(context).registerReceiver(
                onSettingsChanged,
                IntentFilter(SettingsFragment.APP_SETTINGS_CHANGED_BROADCAST)
        )
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.result_fragment, container, false)
        mBinding.recyclerView.adapter = mItemViewAdapter
        return mBinding.root
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(onSettingsChanged)
        super.onDestroy()
    }

    fun addNewResult(palindrome: Palindrome) {
        mPresenter.onReceiveNewResultItem(palindrome)
    }

    private fun injectDependencies() {
        if (activity is MainActivity) {
            (activity as MainActivity).mainComponent
                    ?.plus(ResultContract.Module(this))
                    ?.inject(this)
            mPresenter.onStart()
        }
    }

    override fun refreshTheResultList() {
        mItemViewAdapter.notifyDataSetChanged()
    }

    private val onSettingsChanged = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            mPresenter.loadAppSettings()
        }
    }
}