package com.itc_android.main.input

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itc_android.R
import com.itc_android.databinding.InputFragmentBinding
import com.itc_android.main.MainActivity
import kotlinx.android.synthetic.main.input_fragment.*
import javax.inject.Inject

/**
 * Created by binhnguyen on 9/16/17.
 */
class InputFragment : Fragment(), InputContract.View {
    @Inject lateinit var mPresenter: InputContract.Presenter
    private lateinit var mBinding: InputFragmentBinding
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        injectDependencies()
        handleEvents()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.input_fragment, container, false)
        return mBinding.root
    }

    private fun injectDependencies() {
        if (activity is MainActivity) {
            (activity as MainActivity).mainComponent
                    ?.plus(InputContract.Module(this))
                    ?.inject(this)
        }
    }

    private fun handleEvents() {
        btnSubmit?.setOnClickListener {
            mPresenter.checkPalindrome(edtInput?.text?.toString() ?: "")
        }
    }

    override fun showInvalidInputError() {
        edtInput?.error = getString(R.string.error_emptyInput)
    }
}