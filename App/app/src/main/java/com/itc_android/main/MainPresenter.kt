package com.itc_android.main

import com.itc_android.entity.Palindrome
import javax.inject.Inject

/**
 * Created by binhnguyen on 9/16/17.
 *
 * This class handles the logic in main activity
 */
class MainPresenter @Inject constructor() : MainContract.Presenter {

    @Inject lateinit var mView: MainContract.View

    override fun onComputationFinish(palindrome: Palindrome) {
        mView.showResultView(palindrome)
    }
}