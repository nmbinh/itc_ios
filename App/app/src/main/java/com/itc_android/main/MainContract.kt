package com.itc_android.main

import com.itc_android.entity.Palindrome
import com.itc_android.main.input.InputContract
import com.itc_android.main.result.ResultContract
import com.itc_android.repository.DataModule
import com.itc_android.scope.ActivityScope
import dagger.Provides

/**
 * Created by binhnguyen on 9/16/17.
 */
interface MainContract {
    interface View {
        fun showInputView()
        fun showResultView(palindrome: Palindrome)
    }

    interface Presenter {
        fun onComputationFinish(palindrome: Palindrome)
    }

    @ActivityScope
    @dagger.Component(modules = arrayOf(Module::class, DataModule::class))
    interface Component {
        fun inject(activity: MainActivity)

        fun plus(module: InputContract.Module): InputContract.Component
        fun plus(module: ResultContract.Module): ResultContract.Component
    }

    @dagger.Module
    class Module(private val mView: View) {
        @ActivityScope
        @Provides
        fun provideView() = mView

        @ActivityScope
        @Provides
        fun providePresenter(it: MainPresenter): Presenter = it
    }
}