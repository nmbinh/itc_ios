package com.itc_android.main.result

import com.itc_android.domain.GetAppSettingsUseCase
import com.itc_android.entity.Palindrome
import com.itc_android.main.result.item.ResultItemViewAdapter
import com.itc_android.main.result.item.ResultItemViewModel
import javax.inject.Inject


/**
 * Created by binhnguyen on 9/16/17.
 */
class ResultPresenter @Inject constructor() : ResultContract.Presenter {
    @Inject lateinit var mView: ResultContract.View
    @Inject lateinit var mItemViewAdapter: ResultItemViewAdapter
    @Inject lateinit var mAppSettingsProvider: GetAppSettingsUseCase
    var numOfResults = 5

    override fun onStart() {
        loadAppSettings()

    }
    override fun loadAppSettings(){
        mAppSettingsProvider.run(Unit)
                .doOnNext { numOfResults = it.numOfResults }
                .doOnNext { removeOutOfRangeItems() }
                .subscribe()
    }
    override fun onReceiveNewResultItem(item: Palindrome) {
        val viewModel = ResultItemViewModel(item)
        mItemViewAdapter.addNewItem(viewModel)
        removeOutOfRangeItems()
    }

    private fun removeOutOfRangeItems() {
        while (numOfResults < mItemViewAdapter.itemCount) {
            mItemViewAdapter.removeItem(mItemViewAdapter.itemCount - 1)
        }
    }

}