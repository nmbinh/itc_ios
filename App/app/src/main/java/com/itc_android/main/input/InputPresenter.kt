package com.itc_android.main.input

import com.itc_android.domain.PalindromeCheckingUseCase
import com.itc_android.entity.Palindrome
import com.itc_android.main.MainContract
import javax.inject.Inject

/**
 * Created by binhnguyen on 9/16/17.
 */
class InputPresenter @Inject constructor() : InputContract.Presenter {
    @Inject lateinit var mView: InputContract.View
    @Inject lateinit var mParent: MainContract.Presenter
    @Inject lateinit var mPalindromeChecker: PalindromeCheckingUseCase

    override fun checkPalindrome(string: String) {
        if (string.isEmpty())
            mView.showInvalidInputError()
        else {
            //the computation will run at background thread, and emit result to Android Main Thread.
            mPalindromeChecker.run(string)
                    .doOnNext { onComputationFinish(string, it) }
                    .subscribe()
        }
    }

    private fun onComputationFinish(statement: String, isPadlindrome: Boolean) {
        //Here we invoke to the containing activity to display result on separated screen.
        //In case we want to display result right on this screen, we can change this method to ask
        //the view to display
        mParent.onComputationFinish(Palindrome(statement, isPadlindrome))
    }
}