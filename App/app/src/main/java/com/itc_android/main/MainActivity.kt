package com.itc_android.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.preference.PreferenceManager
import android.view.Menu
import android.view.MenuItem
import com.itc_android.R
import com.itc_android.entity.Palindrome
import com.itc_android.main.input.InputFragment
import com.itc_android.main.result.ResultFragment
import com.itc_android.main.settings.SettingsFragment
import com.itc_android.repository.DataModule
import javax.inject.Inject

/**
 * Created by binhnguyen on 9/16/17.
 */
class MainActivity : AppCompatActivity(), MainContract.View {
    @Inject lateinit var mPresenter: MainContract.Presenter
    var mainComponent: MainContract.Component? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        initDependencies()
        assembleFragments()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.settings, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.menuSettings) {
            //Open settings page
            supportFragmentManager
                    .beginTransaction()
                    .addToBackStack("Settings")
                    .replace(R.id.fragmentContainer, SettingsFragment())
                    .commit()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initDependencies() {
        mainComponent = DaggerMainContract_Component.builder()
                .module(MainContract.Module(this))
                .dataModule(DataModule(applicationContext))
                .build()
        mainComponent?.inject(this)
    }

    private fun assembleFragments() {
        supportFragmentManager.findFragmentByTag(InputFragment::class.java.name)?.let {
            //This is the case activity restarts because of configurations changed
            return
        }
        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, ResultFragment(), ResultFragment::class.java.name)
                .add(R.id.fragmentContainer, InputFragment(), InputFragment::class.java.name)
                .commitNow()
        showInputView()
    }

    override fun showInputView() {
        val inputFragment = supportFragmentManager.findFragmentByTag(InputFragment::class.java.name)
        val resultFragment = supportFragmentManager.findFragmentByTag(ResultFragment::class.java.name)
        supportFragmentManager
                .beginTransaction()
                .hide(resultFragment)
                .show(inputFragment)
                .commit()
    }

    override fun showResultView(palindrome: Palindrome) {
        val inputFragment = supportFragmentManager.findFragmentByTag(InputFragment::class.java.name)
        val resultFragment = supportFragmentManager.findFragmentByTag(ResultFragment::class.java.name)
        supportFragmentManager
                .beginTransaction()
                .addToBackStack("ResultFragment")
                .hide(inputFragment)
                .show(resultFragment)
                .commit()
        (resultFragment as ResultFragment).addNewResult(palindrome)
    }
}