package com.itc_android.main.result.item

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.itc_android.R
import com.itc_android.databinding.ResultItemViewBinding

/**
 * Created by binhnguyen on 9/17/17.
 */
class ResultItemViewAdapter : RecyclerView.Adapter<ResultItemViewAdapter.ResultItemViewHolder>() {

    private val dataList = ArrayList<ResultItemViewModel>()

    fun addNewItem(item: ResultItemViewModel) {
        dataList.add(0, item)
        notifyItemInserted(0)
    }

    fun removeItem(position: Int) {
        if (dataList.size > position) {
            dataList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ResultItemViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val binding: ResultItemViewBinding
        binding = DataBindingUtil.inflate(inflater, R.layout.result_item_view, parent, false)
        return ResultItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ResultItemViewHolder?, position: Int) {
        holder?.bindData(dataList[position])
    }

    class ResultItemViewHolder(private val binding: ResultItemViewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindData(data: ResultItemViewModel) {
            binding.viewModel = data
        }
    }
}