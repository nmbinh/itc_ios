package com.itc_android.domain

import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test


/**
 * Created by binhnguyen on 9/17/17.
 */
class PalindromeCheckingUseCaseTest {
    lateinit var palindromeChecker: PalindromeCheckingUseCase

    @Before
    fun setUp() {
        palindromeChecker = PalindromeCheckingUseCase()
    }

    @Test
    fun buildUseCaseObservable() {
    }

    //region Tests for private method: removeInvalidCharacters
    @Test
    fun removeInvalidCharacters_emptyString_emptyString() {
        Assert.assertSame(removeInvalidCharacters(""), "")
    }

    @Test
    fun removeInvalidCharacters_alphanumberic_returnSame1() {
        val string = "abcdefABCDEF123456789"
        Assert.assertSame(removeInvalidCharacters(string), string)
    }

    @Test
    fun removeInvalidCharacters_alphanumberic_returnSame2() {
        val string = "ABCEDfsdfsdf34234fsdfsdf2342JFLJDFLJF334242sfJFDKFJDSJF"
        Assert.assertSame(removeInvalidCharacters(string), string)
    }

    @Test
    fun removeInvalidCharacters_stringWithWhiteSpaces_removeWhiteSpaces() {
        val input = "abc 123"
        val expect = "abc123"
        Assert.assertEquals(removeInvalidCharacters(input), expect)
    }

    @Test
    fun removeInvalidCharacters_stringWithTabs_removeTabs() {
        val input = "abc        123"
        val expect = "abc123"
        Assert.assertEquals(removeInvalidCharacters(input), expect)
    }

    @Test
    fun removeInvalidCharacters_stringWithSpecialChars_removeNonAlphanumeric() {
        val input = "abc#$%^&*()123#$%^&*()_    @#$%^&"
        val expect = "abc123"
        Assert.assertEquals(removeInvalidCharacters(input), expect)
    }

    @Test
    fun removeInvalidCharacters_onlyNonAlphanumeric_empty() {
        val input = "!@#$%^&*()_-+{        }[]\\//.,~`"
        val expect = ""
        Assert.assertEquals(removeInvalidCharacters(input), expect)
    }

    /*
     * We use reflection to test private method
     */
    private fun removeInvalidCharacters(input: String): Any {
        val method = PalindromeCheckingUseCase::class.java
                .getDeclaredMethod("removeInvalidCharacters", String::class.java)
        method.isAccessible = true
        return method.invoke(palindromeChecker, input)
    }
    //endregion


    //region Tests for private method: checkPalindrome
    @Test
    fun checkPalindrome_emptyString_true() {
        Assert.assertEquals(checkPalindrome(""), true)
    }

    @Test
    fun checkPalindrome_singleCharacter_true() {
        Assert.assertEquals(checkPalindrome("a"), true)
    }

    @Test
    fun checkPalindrome_oddNumberOfCharacters_true() {
        Assert.assertEquals(checkPalindrome("aaa"), true)
    }

    @Test
    fun checkPalindrome_oddNumberOfCharacters_true2() {
        Assert.assertEquals(checkPalindrome("123454321"), true)
    }

    @Test
    fun checkPalindrome_palindromeString_true() {
        Assert.assertEquals(checkPalindrome("madam"), true)
    }

    @Test
    fun checkPalindrome_palindromeString_true2() {
        Assert.assertEquals(checkPalindrome("maddam"), true)
    }

    /*
     * We use reflection to invoke the private method
     */
    fun checkPalindrome(input: String): Any {
        val method = PalindromeCheckingUseCase::class.java
                .getDeclaredMethod("checkPalindrome", String::class.java)
        method.isAccessible = true
        return method.invoke(palindromeChecker, input)
    }
    //endregion

    //region Test for the whole use-case
    @Test
    fun buildUseCaseObservable_assertNoError() {
        val testObserver = TestObserver<Boolean>()
        palindromeChecker.buildUseCaseObservable("").subscribe(testObserver)
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)
    }

    @Test
    fun buildUseCaseObservable_checkEmptyPalindrome_true() {
        val testObserver = TestObserver<Boolean>()
        palindromeChecker.buildUseCaseObservable("").subscribe(testObserver)
        testObserver.assertValue(true)
    }

    @Test
    fun buildUseCaseObservable_checkSampleFromRequirement_true() {
        val testObserver = TestObserver<Boolean>()
        palindromeChecker.buildUseCaseObservable("Red rum, sir, is murder").subscribe(testObserver)
        testObserver.assertValue(true)
    }

    @Test
    fun buildUseCaseObservable_checkSampleFromRequirement_false() {
        val testObserver = TestObserver<Boolean>()
        palindromeChecker.buildUseCaseObservable("Turtles are awesome").subscribe(testObserver)
        testObserver.assertValue(false)
    }
    //endregion
}