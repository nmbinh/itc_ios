## Readme

Here are what I have done:

- I apply the MVP design pattern
- I use Dagger2 to inject dependencies. for example, inject `Presenter` into `Fragment`, inject interactos into `Presenter`
- The application support both portrait and landscape mode
- There is a Settings screen, where user can config the number of result items can be displayed
- Computation is executed at background, asynchornously.
- Unit test for the main logics


---

## Original Requirement:


### Technical Interview Android

#### How to take the test

- Create a **private** [bitbucket](https://bitbucket.org) repository named itc_ios.
- Write your code and push your changes to that repository.
- Write any documentation on how to build your code using the latest version of Android studio.
- Give access to the bitbucket account dario@it-consultis.com once done.

#### Time constraints

You have until Monday 10:00am (Vietnamese time) to perform the test, but if you finish it before this will be taken into account.

#### Description

The goal of the application is to develop an application containing one activity hosting 2 fragments:

- The first fragment (using 'input_fragment' layout) contains one text field and one button.
- Upon submission, the program will solve the following problem:
Given the string in input, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
For example, "Red rum, sir, is murder" is a palindrome, while "Turtles are awesome" is not.
- After the computation, display the second fragment (using 'result_fragment' layout). It will display the text True if it is a palindrome, False otherwise.

Technicalities:

- The choice of any specific development pattern (MVP, MVC, MVVM, etc�) will need to be mentioned (just write the one you choose inside the README.md file)
- The code should contain some basic unit tests
- The code should be clean and well indented

Bonus:

- The application should work both on portrait and landscape modes
- In the result view, display a list of the X last operations, X being a variable set in the settings of the application
- The computation for the palindrome is done asynchronously